# Generated by Django 4.1.5 on 2023-01-29 07:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('messaging', '0003_alter_scheduledmessage_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scheduledmessage',
            name='status',
            field=models.IntegerField(choices=[(100, 'scheduled'), (200, 'done'), (300, 'processing'), (400, 'failed'), (500, 'unknown')], default=100),
        ),
    ]
