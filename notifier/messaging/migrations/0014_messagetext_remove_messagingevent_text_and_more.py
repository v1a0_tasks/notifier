# Generated by Django 4.1.5 on 2023-01-29 16:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('messaging', '0013_remove_messagingevent_type'),
    ]

    operations = [
        migrations.RunSQL("""
                   drop view if exists processing_messages;
                   drop view if exists active_messages;
           """),
        migrations.CreateModel(
            name='MessageText',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.CharField(max_length=512)),
            ],
        ),
        migrations.RemoveField(
            model_name='messagingevent',
            name='text',
        ),
        migrations.AddField(
            model_name='scheduledmessage',
            name='sent_with_text',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='messaging.messagetext'),
        ),
    ]
