# Generated by Django 4.1.5 on 2023-01-29 15:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('addressee', '0003_alter_addressee_operator_code'),
        ('messaging', '0010_activemessages_processingmessages'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='messagingevent',
            name='is_active',
        ),
        migrations.RemoveField(
            model_name='scheduledmessage',
            name='is_active',
        ),
        migrations.AlterField(
            model_name='scheduledmessage',
            name='addressee',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='scheduled_messages', to='addressee.addressee'),
        ),
    ]
