# Ревью проверяющего и конструктивная критика от автора


### Комментарий #1:

Довольно странно смотрится модуль misc.messages, используется он только в приложении addressee, почему бы в нем не
разместить эти сообщения? 

#### Замечание автора:

Стандартная практика выносить текстовые сообщения в один словарь или класс, чтобы не дублировать рукописный текст в 
разных кусках кода (как минимум во избежание опечаток). И если в будущем потребуется добавить поддержку разных язычков (ru/en),
то без подобных решений не обойтись, почему бы не заложить эту функциональность заранее, когда это не требует больших усилий?


### Комментарий #2:

Есть индексы на некоторые поля, но особо с фильтрами не совпадают


#### Замечание автора:

  - Индексы используются (!!!) в селектах представлений. Это самое "жирное" на производительность место сервиса.
  - Большинство фильтров в сервисе идут по id, для которого индекс Postgres создает по умолчанию. 
Добавление индексов для остальных не принесет большого прироста, т.к. при малых объёмах таблиц postgres планировщик
прибегнет к SeqScan игнорируя индексы.

file: `notifier/messaging/migrations/0017_update_views.py`

```sql
Create or replace view active_messages AS (
    SELECT
        row_number() over (order by event.id, msg.id) as id,

        event.id as event_id,
        event.title as event_title,
        event.stop_at as stop_at,

        txt.id as text_id,
        COALESCE(txt.text, '(no data)') as text,

        msg.id as message_id,
        msg.status as status,
        msg.updated_by_task as updated_by_task,

        adr.id as addressee_id,
        adr.phone_number as phone_number
    FROM messaging_messagingevent AS event
    INNER JOIN messaging_scheduledmessage msg on event.id = msg.event_id
    LEFT JOIN addressee_addressee adr on msg.addressee_id = adr.id
    LEFT JOIN messaging_messagetext txt on event.text_id = txt.id 
    WHERE (
        msg.status < 300
        AND (event.start_at <= now()) -- INDEX FOR start_at IN USE
        AND (event.stop_at > now())   -- INDEX FOR stop_at IN USE
    )
    ORDER BY msg.status desc, event.id
);
 
Create or replace view processing_messages AS (
    SELECT
        row_number() over (order by event.id, msg.id) as id,

        event.id as event_id,
        event.title as event_title,
        event.stop_at as stop_at,
     
        txt.id as text_id,
        COALESCE(txt.text, '(no data)') as text_value,

        msg.id as message_id,
        msg.status as status,
        msg.updated_by_task as updated_by_task,

        adr.id as addressee_id,
        adr.phone_number as phone_number
    FROM messaging_messagingevent AS event
    INNER JOIN messaging_scheduledmessage msg on event.id = msg.event_id
    LEFT JOIN addressee_addressee adr on msg.addressee_id = adr.id
    LEFT JOIN messaging_messagetext txt on event.text_id = txt.id 
    WHERE (
        msg.status = 300
        AND (event.start_at <= now()) -- INDEX FOR start_at IN USE
        AND (event.stop_at > now())   -- INDEX FOR stop_at IN USE
    )
    ORDER BY msg.status desc, event.id
)
```

### Комментарий #3:

У модели MessagingEvent есть свойство is_active и логики в нём странная и не отражающая название, хоть он и не используется

#### Замечание автора:

is_active - рудиментарное свойство, которое осталось исторически в ходе разработки. 
Раньше использовалось в логике сервиса, в последствии, логику выпилил, а поле осталось.
Его можно убрать, можно оставить, занимает оно 5 строк.
Оно ни как не влияет на работу сервиса. 


Читаем построчно "странную и не отражающая название логику":

```python
@property                       # свойство объекта "рассылка"
def is_active(self):            # она_активна
    if not self.stop_at:        # Если: время остановки у рассылки не указано (в принципе)
        return False            #   Ответ: рассылка не активна
    return now() > self.stop_at # Иначе: проверить не слишком поздно ли слать рассылку, и вернуть ответ. 
                                # Не поздно - активна, поздно - не активна
```

### Комментарий #4:

У модели MessagingEvent странный метод count_scheduled_messages, даже не свойство.


#### Замечание автора:

Очередной рудиментарный кусочек в коде на 2 строчки (!!!), 
который не мешает работе программы и просто перестал использоваться.

Почему это не свойство? По тому, что идет ресурсозатратное обращение к базе и разработчик должен понимать, что 
это не обращение к легкой property, а вызов запроса подсчета из базы.

`count_scheduled_messages` - "подсчитать запланированные сообщения", это буквально процесс подсчета, и его нужно вызывать.


### Комментарий #5:

Есть модель сообщения ScheduledMessage, и есть ещё две модели идентичные модели ProcessingMessages и ActiveMessages, перебор с таблицами, можно было бы обойтись одной таблицей

#### Замечание автора:

ОЧЕНЬ грубая ошибка для проверяющего. Модели ProcessingMessages и ActiveMessages ссылаются не на **ТАБЛИЦЫ**, а на **ПРЕДСТАВЛЕНИЯ**.
А эти представления, в свою очередь, берут данные **ЕДИНСТВЕННОЙ** базовой таблицы `messaging_messagingevent` 
и просто join-ят к ним данные из связанных таблиц.

Тем самым получается избавиться от огромных и уродливых конструкций ORM и обратиться **КАК БУДТО** к отдельной таблице, в
которой находятся уже только нужные нам данные. А именно "Обрабатываемые" (ProcessingMessages) 
и "Активные" (ActiveMessages) сообщения.

Так что... "можно было бы обойтись одной таблицей" - здесь и так одна таблица.


### Комментарий #6:

Не получилось создать абстрактную модель

#### Замечание автора:

Не может быть минусом к заданию, т.к. это не являлось ни основным, ни дополнительным заданием, а было привнесено мной лично,
а в последствии вырезано и просто закомментировано, с целью не тратить время на отладку того, что не требуется в задании.


### Комментарий #7:

Много миграций

#### Замечание автора:

Какое негативное отражение это имеет на проекте?

В процессе разработки код постоянно покрывается разного рода изменениями и при этом необходимо иметь функциональность 
для перехода работающего сервиса от старой версии к новой. Для поддержания актуальной структуры БД
используются миграции и в проекте их именно столько, сколько потребовала разработка. 

Разумеется можно склеивать и прятать миграции разных исторических периодов в один, но зачем?

Это повлечет лишь раздувание одного файла с "Супер-миграцией", понять смысл которой невозможно будет ни по названию файла,
ни по содержимому, так как удержать всю его логику в голове будет невозможно.

По этому лучше позволить каждому файлу миграции отражать свой маленький и простой смысл. KeepItSimple.

